import { Recipe } from "./recipe.model";
import {Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Subject } from "rxjs/Subject";

@Injectable()
export class RecipeService{
    recipesChanged= new Subject<Recipe[]>();
    private recipes: Recipe[] = [
        new Recipe('Monastir', 'This is a simple discription', 
        'https://s.iha.com/00158058586/Monastir-Les-plages-sur-le-littoral-de-monastir.jpeg',[
            new Ingredient('tenor',1),
            new Ingredient('mamachime',15),
            new Ingredient('tenor',1),
            new Ingredient('mamachime',15),      new Ingredient('tenor',1),
            new Ingredient('mamachime',15),      new Ingredient('tenor',1),
            new Ingredient('mamachime',15),      new Ingredient('tenor',1),
            new Ingredient('mamachime',15),
        ]
    ),
        new Recipe('sousse', 'This is a simple discription',
         'https://www.alibabuy.com/photos/library/1500/14216.jpg',
[
    new Ingredient('dsq',1),
    new Ingredient('dqs',15)

]
        )

    ];
    constructor (private slService: ShoppingListService){}
    getRecipes(){
        return this.recipes.slice();
    }
    getRecipe(index:number){
        return this.recipes.slice()[index]
    }
    addIngrendientsToShoppingList(ingredients:Ingredient[]){
      this.slService.addIngredients(ingredients)
      }
    addRecipe(recipe:Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }
    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());

    }
    deleteRecipe(index:number) {
        this.recipes.splice(index,1);
        this.recipesChanged.next(this.recipes.slice());
    }
}