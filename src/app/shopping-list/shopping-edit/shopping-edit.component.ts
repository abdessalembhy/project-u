import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
} from '@angular/core';

import { Ingredient } from '../../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('f') slForm : NgForm;
  subscription: Subscription;
  editMode= false;
  editItemIndex: number;
  editItem: Ingredient;

  constructor(private shoopingListService: ShoppingListService) { }

  ngOnInit() {
   this.subscription=  this.shoopingListService.startedEditing
    .subscribe(
      (index:number) => {
        this.editItemIndex = index;
        this.editMode = true;
        this.editItem= this.shoopingListService.getIngredient(index);
        this.slForm.setValue({
          name: this.editItem.name,
          amount : this.editItem.amount
        })
      }
    );
  }

  onSubmit(form: NgForm) { 
    const value= form.value;
    const newIngredient = new Ingredient(value.name, value.amount);
    if(this.editMode==true){
      this.shoopingListService.updateIngredient(this.editItemIndex, newIngredient)
      this.editMode=false;
    }
    else {
      this.shoopingListService.addIngredient(newIngredient)
    }
    this.slForm.reset();
  }
  onclear(){
    this.slForm.reset();
    this.editMode=false;
  }
  onDelete(){
    this.onclear;
    this.shoopingListService.deleteIngredient(this.editItemIndex);
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
 
}
