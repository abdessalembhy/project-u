import { Component, OnInit, OnDestroy } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
  providers: []
})
export class ShoppingListComponent implements OnInit, OnDestroy{
  ingredients=Ingredient[''];
private subscription : Subscription;
  constructor(private shoopingListService: ShoppingListService) { }

  ngOnInit() {
    this.ingredients=this.shoopingListService.getIngrendients();
    this.subscription = this.shoopingListService.ingredientsChanged
    .subscribe(
      (ingredients: Ingredient[]) =>
      this.ingredients=ingredients
    )
  }

  onEditItem(index:number) {
    this.shoopingListService.startedEditing.next(index)
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  
}
